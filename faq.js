var faqcontentHeight = document.getElementById("faq-content").clientHeight;
var windowHeight = window.screen.height;
var faqnavbarHeight = document.getElementById("faq-navbar").style.height;
document.getElementById("faq-navbar").style.height = document.getElementById("faq-content").clientHeight+'px';

mediaQuery = window.matchMedia('(orientation: portrait)');
if (mediaQuery.matches) {
    document.getElementById("faq-navbar").style.height = '0px';
    document.getElementById("faq-content").style.backgroundSize = document.getElementById("faq-content").clientHeight;

}
function readmore(self) {
    self.closest(".question-answer-section").getElementsByClassName("hide-faq-content").item(0).classList.add("show-faq-content");
    self.classList.add("hide-faq-button");
    self.closest(".question-answer-section").getElementsByClassName("readless-button").item(0).classList.add("show-faq-button");
}
function readless(self) {
    self.closest(".question-answer-section").getElementsByClassName("hide-faq-content").item(0).classList.remove("show-faq-content");
    self.classList.remove("show-faq-button");
    self.closest(".question-answer-section").getElementsByClassName("readmore-button").item(0).classList.remove("hide-faq-button");
}


function highlightedSection(self, id) {
    var navmenu = document.getElementsByClassName("faq-navigation-menu").item(0);
    if (navmenu.classList.contains("expand-navbar")) {
        var positionGeneral = document.getElementById("generalSection").getBoundingClientRect().y;
        var positionselfy = self.getBoundingClientRect().y;
        var differencePositionY = positionGeneral - positionselfy;
        document.getElementsByClassName("active-nav-menu").item(0).classList.remove("active-nav-menu");
        self.classList.add("active-nav-menu");
        document.getElementsByClassName("faq-nav-position").item(0).style.top = differencePositionY + "px";
        navmenu.classList.remove("expand-navbar");
        document.getElementsByClassName("show-faq-content").item(0).classList.remove("show-faq-content");
        document.getElementById(id).classList.add("show-faq-content");
        document.getElementById('arrow-toggle').classList.toggle('arrow-up-rotate');

    }
    else {
        document.getElementsByClassName("active-nav-menu").item(0).classList.remove("active-nav-menu");
        self.classList.add("active-nav-menu");
        document.getElementsByClassName("show-faq-content").item(0).classList.remove("show-faq-content");
        document.getElementById(id).classList.add("show-faq-content");
        document.getElementById('arrow-toggle').classList.add('arrow-up-rotate');
        navmenu.classList.add("expand-navbar");
        document.getElementsByClassName("faq-nav-position").item(0).style.top = "0px";
    }
    document.getElementById("faq-navbar").style.height = document.getElementById("faq-content").clientHeight + 'px';

}

function dropdown() {
    document.getElementsByClassName("active-nav-menu").item(0).click();
    
}
